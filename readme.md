###安装:
`npm install --save mbook`
***
###使用
```
import mbook from 'mbook'
book.init(
				{
				target:document.getElementById('ye'),
				url:'./all.txt',  //url,target为必传值
				row:40,
				col:25,//设置行列
				drution:500,//翻页动画时长设置
				bg:'wheat',、、设置背景颜色默认wheat
				fsize:'15px',//设置字体大小
			fontFamily:'Georgia'//设置字体类型
				},function(data){//读取文本完毕返回参数
				//data包含所有的目录，总页数
			    }
				)
```
***
###事件监听
```
 book.on({
 			'finsh':function(){ //监听翻完整本书的事件
 				alert('finsh')
 			},
 			 'firstPage':function(){//监听上翻到第一页的事件
 			 	alert('one')
 			 },
 			 'nextPage':function(data){//监听下翻页的事件，返回当前页码
 			 	console.log(data)
 			 },
 			 'prevPage':function(data){//监听上翻页的事件，返回当前页码
 			 	console.log(data)
 			 }
 			})		
```	
***
###页面跳转
```
 book.jump()//下翻页
 book.prev()//上翻页
``` 
***
###版本说明
1. 动画改为css3的过渡，并禁止用户在动画期间翻页。
2. 动画的时间可以由用户设置。
 