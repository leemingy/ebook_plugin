var book = (function() {
	var num = 0,
		zind = 2,
		z = 0,
		start = 0,
		zs = 0;
	var book = {
		bookdata: {
			title: [],
			contents: [],
			yes: [],
			tyes: [],
			row: 30,
			target:null,
			col: 15,
			obj: null,
			bg:'wheat',
			canvasDom: null,
			canvasContext: null,
			fsize: 'px',
			fontFamily: 'Georgia',
			fontNum: 450,
			drution:500,
			ani:true,
			getTot: function() {
				var tot = 0;
				this.yes.forEach(function(val, index) {
					tot += val;
				})
				return tot
			},
			init: function(obj) {
				obj.row ? this.row = obj.row : this.row = this.row;
				obj.col ? this.col = obj.col : this.col = this.col;
				obj.fsize ? this.fsize = obj.fsize : this.fsize = this.fsize;
				obj.fontFamily ? this.fontFamily = obj.fontFamily : this.fontFamily = this.fontFamily;
				obj.bg? this.bg = obj.bg : this.bg = this.bg
				obj.drution?this.drution = obj.drution : this.drution=this.drution
				this.target = obj.target
				this.obj = obj
				this.fontNum = this.row * this.col
				this.setTyes()
				this.cretaAlldom()
				this.cretaYe(0, 0)
			},
			setTyes: function() {
				var tot = 0
				for(var i = 0; i < this.yes.length; i++) {
					tot += this.yes[i]
					this.tyes.push(tot)
				}
			},

			cretaAlldom: function() {
console.log('ccc')
				var ocan = document.createElement('canvas');
				//							var ocon = ocan.getContext('2d');
				ocan.setAttribute('class', 'm_canvas_dom')
				//							ocan.style.border = '1px solid black'
				ocan.width = this.target.offsetWidth;
				ocan.height = this.target.offsetHeight;
				ocan.style.position = 'absolute';
				ocan.style.backgroundColor = this.bg
				ocan.style.zIndex = 1;
				this.target.appendChild(ocan)
				this.canvasDom = ocan
				this.canvasContext = ocan.getContext('2d')

			},
			cretaYe: function(i, page) {
				var str = this.contents[i].substr(this.fontNum * page, this.fontNum);
				//		
				this.canvasContext.clearRect(0, 0, this.canvasDom.width, this.canvasDom.height)

				this.canvasContext.font = this.fsize + " " + this.fontFamily;
				var ind = 0;
				for(var k = 0; k < this.row; k++) {
					for(var j = 0; j < this.col; j++) {
						str[ind] ? this.canvasContext.fillText(str[ind], this.canvasDom.width / this.col * j, this.canvasDom.height / this.row * (k + 1) - 2) : '';
						ind++;
					}
				}

			}

		},
		getmulu: function() {

			var mulus = []
			for(var i = 0; i < this.bookdata.title.length; i++) {
				var str = this.bookdata.title[i].replace('------------', '')
				mulus.push(str)

			}
			return mulus
		},
		jumpMlu: function(j) {
			book.bookdata.cretaYe(j, 0)
			var tot = 0
			for(var k = 0; k < j; k++) {
				tot += book.bookdata.yes[k]
			}
			num = tot
			start = tot
			zs = j - 1
		},
		on: function(obj) {
			for(var i in obj) {
				this.events[i] = obj[i]
			}

		},
		emit: function(name, data) {

			this.events[name](data)

		},
		events: {},
		init: function(obj, callback) {
			var row, col
			obj.row ? row = obj.row : row = book.bookdata.row
			obj.col ? col = obj.col : col = book.bookdata.col
			var xhr = new XMLHttpRequest()
			obj.url ? '' : console.error('请传入url')
			obj.target? '' : console.error('请传入节点')
			xhr.open('get', obj.url, true)
			xhr.onreadystatechange = function() {
				if(xhr.readyState == 4 && xhr.status == 200) {
					var test = /------------\s+.+/g
					var txt = xhr.responseText
					book.bookdata.title = txt.match(test)
					book.bookdata.contents = txt.split('------------')
					book.bookdata.contents.shift()

					for(var i = 0; i < book.bookdata.contents.length; i++) {
						book.bookdata.yes.push(Math.ceil(book.bookdata.contents[i].length / (obj.row * obj.col)))
					}
					book.bookdata.init(obj)
					callback({
						mulu: book.getmulu(),
						totalPage: book.bookdata.getTot(),

					})
					//                  
				}
			}
			xhr.send()
		},
		jump: function() {
			
			if(book.bookdata.ani){
				book.bookdata.ani = false
			num++
			if(num > book.bookdata.getTot() - 1) {

				num = book.bookdata.getTot() - 1;
				this.emit('finsh')
				book.bookdata.ani = true
				return
			}
			this.emit('nextPage', {
				page: num + 1
			})
           this.createAni('m_newdom',function(classname){
            	document.getElementsByClassName(classname)[0].style.left = document.getElementsByClassName(classname)[0].offsetLeft-book.bookdata.target.offsetWidth+'px'
                setTimeout(function(){
               	document.getElementsByClassName(classname)[0].remove()
               	book.bookdata.ani = true
               },book.bookdata.drution)
//          
           })
           
			for(var j = 0; j < book.bookdata.tyes.length; j++) {
				if(num == book.bookdata.tyes[j]) {
					if(j == book.bookdata.tyes.length - 1) {
						return
					} else {
						start = book.bookdata.tyes[j]
						zs = j
					}
				}
			}
			book.bookdata.cretaYe(zs + 1, num - start)
			}
		},
		
		prev: function() {
			if(book.bookdata.ani){
				book.bookdata.ani = false
			num--;
			if(num < 0) {
				num = 0
				this.emit('firstPage')
				book.bookdata.ani = true
				return
			}
			this.emit('prevPage', {
				page: num + 1
			})
			this.createAni('m_newdom2',function(classname){
            	document.getElementsByClassName(classname)[0].style.left = document.getElementsByClassName(classname)[0].offsetLeft+book.bookdata.target.offsetWidth+'px'
               setTimeout(function(){
               	document.getElementsByClassName(classname)[0].remove()
               	book.bookdata.ani = true
               },book.bookdata.drution)
			})
			for(var j = 0; j < this.bookdata.tyes.length; j++) {

				//页码最小为一
				if(num + 1 == this.bookdata.tyes[j]) {
					start = book.bookdata.tyes[j - 1] //因为j是上一章的索引所以减一
					zs = j - 1
				}
			}
			book.bookdata.cretaYe(zs + 1, num - start) //zs最小值为0所以加一
}
		},
		createAni:function(classname,callback){
			var can = document.createElement('canvas')
            can.style.zIndex = '2'
            can.style.position = 'absolute'
            can.style.left = 0
            can.style.top = 0
            can.style.transition = 'all '+book.bookdata.drution+'ms'
            can.style.backgroundColor = this.bookdata.bg
            can.setAttribute('class',classname)
            var ocan = can.getContext('2d')
            can.width = this.bookdata.canvasDom.width
            can.height = this.bookdata.canvasDom.height
            var imgdata = this.bookdata.canvasContext.getImageData(0,0,can.width,can.height)
            ocan.putImageData(imgdata,0,0)
            this.bookdata.target.appendChild(can)
            callback(classname)
           
		}
	}
	return book
})(window, document)

export default book
