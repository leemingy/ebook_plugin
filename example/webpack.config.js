const webpack = require('webpack')
const htmlwebpack = require('html-webpack-plugin')
const copywebpack = require('copy-webpack-plugin')
module.exports = {
	entry:{index:'./index.js'},
	output:{
		path:__dirname+'/dist/',
		filename:'[name].js'
	},
	devServer:{
		port:8080,
		hot:true,
		inline:true,
//		open:true,
		contentBase:__dirname+'/dist/',
	
	},
	module:{
	  rules:[
	    {
	    	test:/\.js$/,
	    	loader:'babel-loader'
	    }
	  ]
	},
	plugins:[
	
	  new htmlwebpack({
	  	template:'./index.html',
	  	chunks:['index']
	  }),
	  new copywebpack([
	  	{
	  	from:'./static',
	  	to:'./static'
	  }
	  ]),
	  new webpack.HotModuleReplacementPlugin()
	]
}
